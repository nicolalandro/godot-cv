[![gitlab pages deployed page link](https://img.shields.io/badge/Gitlab-Pages-orange?logo=gitlab)](https://nicolalandro.gitlab.io/godot-cv/)

# Project
That project aim to use godot game.

![Game screenshot](doc/sample.png)

## Environment
For develop that project I use the following softwares
* Godot_v4.2.1-stable_linux.x86_64
* NODE JS v20.10.0
* NPM/npx 10.2.3

## Serve builded html
```
cd public
npx local-web-server --cors.embedder-policy "require-corp" --cors.opener-policy "same-origin" --directory .
# if you use the coi-serviceworker fix you can simplu test with
npx local-web-server --directory .
```
## Deploy
* Test gitlab pages for deploy but seams not load correctly also with coi-serviceworker

## 3D Resources
* octopus: https://sketchfab.com/3d-models/octopus-3e08df9bc33f462bafe1fb42110e8e88
* pirate boat: https://sketchfab.com/3d-models/pirate-ship-6b32fb0dac4c4e79a2a09a93559302e8
* sky: https://polyhaven.com/a/kloppenheim_06_puresky
* mountain: https://sketchfab.com/3d-models/low-poly-mountain-scene-3c6a3e8c4d0a4c4aaa5dfbbd91107aa4 
* some islands: https://blendswap.com/blend/20283

# References
* Godot Engine
* Fix coarse error: https://github.com/gzuidhof/coi-serviceworker explained also [here](https://stackoverflow.com/questions/68609682/is-there-any-way-to-use-sharedarraybuffer-on-github-pages)
* study wather: https://github.com/CBerry22/Buoyancy-in-Godot-4
