extends MeshInstance3D

var speed = 0.1
var rotate_speed = 1
@onready var panel1 = $"../../Panel1"
@onready var panel2 = $"../../Panel2"

func _process(delta):
	if Input.is_action_pressed("up"):
		self.translate(Vector3.FORWARD * speed)
	if Input.is_action_pressed("down"):
		self.translate(-Vector3.FORWARD * speed)
	if Input.is_action_pressed("left"):
		self.rotation_degrees.y += rotate_speed
	if Input.is_action_pressed("right"):
		self.rotation_degrees.y -= rotate_speed

func _on_area_3d_area_entered(area):
	#print("collide in")
	if area.name == 'potion_collider':
		#print("show potion")
		panel1.visible = true
	if area.name == 'isle_collider':
		panel2.visible = true


func _on_area_3d_area_exited(area):
	#print("collide out")
	if area.name == 'potion_collider':
		#print("unshow potion")
		panel1.visible = false
	if area.name == 'isle_collider':
		panel2.visible = false

