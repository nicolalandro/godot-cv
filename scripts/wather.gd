extends MeshInstance3D

var max = 1
var change = 0.0001
var sign = 1
var material = null

func _ready():
	material = get_surface_override_material(0)
	
func _physics_process(delta):
	var uv1_offset = material.uv1_offset
	var actual = uv1_offset[0]
	if actual > max:
		sign = -1 
	if actual < -max:
		sign = 1
	var c = actual + sign * change
	material.uv1_offset = Vector3(c, c, 0.)
